const express = require('express');
const router = express.Router();
const models = require('../models')

/* GET connection page */
// router.get('/login', function(req, res, next) {
//     res.render('index', { title: 'NETFLIX' });
    
  
// });
const passport = require('passport')
, LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
function(username, password, done) {
  User.findOne({ username: username }, function(err, user) {
    if (err) { return done(err); }
    if (!user) {
      return done(null, false, { message: 'Incorrect username.' });
    }
    if (!user.validPassword(password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }
    return done(null, user);
  });
}
));

router.post('/login',
  passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/connection/login',
                                   failureFlash: true })
);
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'passwd'
  },
  function(username, password, done) {
    // ...
  }
));


module.exports = router;